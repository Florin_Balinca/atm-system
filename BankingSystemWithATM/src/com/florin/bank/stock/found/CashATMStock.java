/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.stock.found;

import com.florin.bank.business.Money;
import java.util.List;

/**
 *
 * @author Florin0
 */
public interface CashATMStock {

    public boolean isMoneyAvailable(Money cash);

    public boolean hasATMFounds();
    
    public List<Money> deliverMoney(double anmount);
}
