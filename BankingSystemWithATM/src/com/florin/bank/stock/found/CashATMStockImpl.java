/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.stock.found;


import com.florin.bank.business.Money;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Florin0
 */
public class CashATMStockImpl implements CashATMStock {

    ArrayList<ArrayList<Money>> cashStocks = new ArrayList<ArrayList<Money>>();
    Money[] cash = new Money[6];

    public CashATMStockImpl() {
        cash[0] = new Money("EUR", 10);
        cash[1] = new Money("EUR", 50);
        cash[2] = new Money("EUR", 100);
        cash[3] = new Money("EUR", 200);
        cash[4] = new Money("EUR", 500);
        cash[5] = new Money("EUR", 1000);
        for (Money cash1 : cash) {
            cashStocks.add(this.addCashInStock(cash1, 150));
        }
    }

    private ArrayList<Money> addCashInStock(Money cash, int n) {
        ArrayList<Money> array = new ArrayList<Money>();
        for (int i = 0; i < n; i++) {
            array.add(cash);
        }
        return array;
    }

    @Override
    public boolean isMoneyAvailable(Money cash) {
        boolean availability = false;
        if(this.cashStocks.get(0).contains(cash)) {
            availability =true;
        } else if (this.cashStocks.get(1).contains(cash)) {
            availability = true;
        } else if (this.cashStocks.get(2).contains(cash)) {
            availability = true;
        } else if (this.cashStocks.get(3).contains(cash)) {
            availability = true;
        } else if (this.cashStocks.get(4).contains(cash)) {
            availability = true;
        } else availability = this.cashStocks.get(5).contains(cash);
        return availability;
    }

    @Override
    public boolean hasATMFounds() {
        return cashStocks != null;
    }

    @Override
    public List<Money> deliverMoney(double anmount) {
        ArrayList<Money> moneyList = new ArrayList<Money>();
        double sumElements = 0;
        if (anmount == 0) {
            System.out.println("You don't have a anmount!");
        } else {
            for (int i = 0; i < cashStocks.get(0).size(); i++) {
                for (int j = i + 1; j <= cashStocks.get(0).size(); j++) {
                    sumElements = sumElements + cash[0].getValue();
                    if (sumElements <= anmount && isMoneyAvailable(cash[0])) {
                        moneyList.add(cash[0]);                        
                    }                    
                }
            }
        }
        return moneyList;
    }
}
