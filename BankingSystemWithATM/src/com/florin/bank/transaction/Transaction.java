/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.transaction;

/**
 *
 * @author Florin0
 */
public class Transaction {

    private String name;

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public Transaction(String name) {
        this.setName(name);
    }

    @Override
    public String toString() {
        return this.getName();
    }

}
