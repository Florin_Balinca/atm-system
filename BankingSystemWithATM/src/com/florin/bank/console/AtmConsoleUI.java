/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.console;

import com.florin.bank.business.Atm;
import com.florin.bank.business.Bank;
import com.florin.bank.business.Card;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Florin0
 */
public class AtmConsoleUI {
    

    public int readIdOfCard() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String intValue = br.readLine();
        int idOfCard = Integer.parseInt(intValue);
        return idOfCard;
    }

    public Card initCard(int id) {        
        Card card = new Card();
        Connection c = null;
        Statement s = null;
        ResultSet rs = null;
        try {
            String dbURL = "jdbc:mysql://localhost/sistembancar";
            String username = "root";
            String password = "";
            String sql = "SELECT * FROM " + Atm.atm.getEntityBankOfATM() + " WHERE id = " + id;
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection(dbURL, username, password);
            s = c.createStatement();
            rs = s.executeQuery(sql);
            while (rs.next() && rs.getDouble("SoldCurent") != 0) {
                Double soldCurent = rs.getDouble("SoldCurent");
                short pin = rs.getShort("PIN");
                String tipCard = rs.getString("TipCard");
                String numeCard = rs.getString("TitularCont");
                String deadlineCard = rs.getString("DataExpirarii");
                card = new Card(new Bank(Atm.atm.getEntityBankOfATM()), numeCard, tipCard, pin, soldCurent, deadlineCard, id);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AtmConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null && rs != null && s != null) {
                try {
                    c.close();
                    rs.close();
                    s.close();
                } catch (SQLException sqlEx) {
                    System.out.println(sqlEx.getMessage());
                }
            }
        }
        return card;
    }
}
