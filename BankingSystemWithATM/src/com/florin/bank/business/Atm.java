/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

import com.florin.bank.device.io.CardReader;
import com.florin.bank.device.io.Display;
import com.florin.bank.device.io.Keyboard;
import com.florin.bank.device.io.Printer;
import com.florin.bank.stock.found.CashATMStock;
import com.florin.bank.stock.found.CashATMStockImpl;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Florin0
 */
public class Atm {

    private CashATMStock stock = new CashATMStockImpl();
    public Keyboard keyboard = new Keyboard();
    public Printer printer = new Printer();
    public CardReader cardReader = new CardReader();
    public Display display = new Display();
    public static Atm atm = new Atm("BRD"); //for defining the design pattern ;)
    private String entityBankOfATM;

    public String getEntityBankOfATM() {
        return entityBankOfATM;
    }

    public final void setEntityBankOfATM(String entityBankOfATM) {
        this.entityBankOfATM = entityBankOfATM;
    }

    public Atm(String entityBankOfATM) {
        this.setEntityBankOfATM(entityBankOfATM);
    }

    public boolean isCardInDatabase(Card card) {
        String dbURL = "jdbc:mysql://localhost/sistembancar";
        String username = "root";
        String password = "";
        String sql = "SELECT * FROM " + this.getEntityBankOfATM() + " WHERE id = " + card.getId();
        Connection c = null;
        Statement s = null;
        ResultSet rs = null;
        boolean found = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection(dbURL, username, password);
            s = c.createStatement();
            rs = s.executeQuery(sql);
            if (rs.next() && rs.getString("TitularCont").equals(card.getName())
                    && rs.getInt("ID") == card.getId()
                    && rs.getString("DataExpirarii").equals(card.getDataExpired())
                    && rs.getString("TipCard").equals(card.getTypeCard())
                    && rs.getShort("PIN") == card.getPin()
                    && rs.getDouble("SoldCurent") == card.getMoneyAvailable()) {
                System.out.println("Your card is found in our DataBase");
                found = true;
            } else {
                System.out.println("We cannot find this card for you, take back your card!\n" + cardReader.returnCard(card));
            }
        } catch (ClassNotFoundException cnfEx) {
            System.out.println("This class has not found!" + cnfEx.getMessage());
        } catch (SQLException sEx) {
            sEx.getMessage();
        } finally {
            if (rs != null && s != null && c != null) {
                try {
                    rs.close();
                    s.close();
                    c.close();
                } catch (SQLException sqlEx) {
                    System.out.println(sqlEx.getMessage());
                }
            }
        }
        return found;
    }

    public CardAndMoney withdrawCash(Card card, Double anmount) {
        CardAndMoney cardAndMoney = new CardAndMoney();
        double balance = card.getMoneyAvailable();
        if (anmount <= balance && anmount >= 0 && anmount % 10 == 0) {
            balance = balance - anmount;
            String sql = "UPDATE " + this.getEntityBankOfATM() + " SET soldCurent = " + balance + " WHERE id = " + card.getId();
            String dbURL = "jdbc:mysql://localhost/sistembancar";
            String username = "root";
            String password = "";
            Statement s = null;
            Connection c = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                c = DriverManager.getConnection(dbURL, username, password);
                s = c.createStatement();
                s.executeUpdate(sql);
            } catch (SQLException | ClassNotFoundException sqlEx) {
                System.out.println(sqlEx.getMessage());
            } finally {
                if (s != null && c != null) {
                    try {
                        s.close();
                        c.close();
                    } catch (SQLException sq) {
                        System.out.println(sq.getMessage());
                    }
                }
            }
        }
        List<Money> moneyList = new ArrayList<Money>();
        boolean hasMoney = stock.hasATMFounds();
        if (hasMoney) {
            moneyList = stock.deliverMoney(anmount);
        } else {
            cardAndMoney = new CardAndMoney("No found available");
        }
        cardAndMoney = new CardAndMoney(card, moneyList, "Thank you for transaction");
        return cardAndMoney;
    }

    public void querryBalance(double curentBalance) {
        System.out.println("Your balance is: " + curentBalance);
    }

    public void transferMoney(Card cardSource, Double transferValue, Card cardDestination) {
        double balanceCardSource = cardSource.getMoneyAvailable();
        double balanceCardDestinantion = cardDestination.getMoneyAvailable();
        if (transferValue <= balanceCardSource && transferValue % 10 == 0) {
            balanceCardSource = balanceCardSource - transferValue;
            balanceCardDestinantion = balanceCardDestinantion + transferValue;
            cardDestination = new Card(new Bank(this.getEntityBankOfATM()), cardDestination.getName(), cardDestination.getTypeCard(), cardDestination.getPin(), balanceCardDestinantion, cardDestination.getDataExpired(), cardDestination.getId());
            String firstSqlStatement = "UPDATE " + this.getEntityBankOfATM() + " SET soldCurent = " + balanceCardSource + " WHERE id = " + cardSource.getId();
            String secondSqlStatement = "UPDATE " + this.getEntityBankOfATM() + " SET soldCurent = " + balanceCardDestinantion + " WHERE id = " + cardDestination.getId();
            String dbURL = "jdbc:mysql://localhost/sistembancar";
            String username = "root";
            String password = "";
            Statement s = null;
            Connection c = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                c = DriverManager.getConnection(dbURL, username, password);
                s = c.createStatement();
                s.executeUpdate(firstSqlStatement);
                s.executeUpdate(secondSqlStatement);
            } catch (SQLException | ClassNotFoundException sqlEx) {
                System.out.println(sqlEx.getMessage());
            } finally {
                if (s != null && c != null) {
                    try {
                        s.close();
                        c.close();
                    } catch (SQLException sq) {
                        System.out.println(sq.getMessage());
                    }
                }
            }
            System.out.println("Transfer Complete :)");
        }
    }
}
