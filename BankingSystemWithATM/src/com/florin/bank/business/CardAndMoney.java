/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

import java.util.List;

/**
 *
 * @author Florin0
 */
public class CardAndMoney {

    private Card card;
    private List<Money> cash;
    private String message;

    public String getMessage() {
        return message;
    }

    public final void setMessage(String message) {
        this.message = message;
    }

    public Card getCard() {
        return card;
    }

    public final void setCard(Card card) {
        this.card = card;
    }

    public List<Money> getCash() {
        return cash;
    }

    public final void setCash(List<Money> cash) {
        this.cash = cash;
    }

    public CardAndMoney(Card card, List<Money> cash,String message) {
        this.setCard(card);
        this.setCash(cash);
        this.setMessage(message);
    }
    
    public CardAndMoney(String message) {
        this.setMessage(message);
    }

    public CardAndMoney() {
        card = null;
        cash = null;
        message = "";
    }
}
