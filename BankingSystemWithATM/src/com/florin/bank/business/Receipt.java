/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

/**
 *
 * @author Florin0
 */
public class Receipt {
    
    private String nameClient;
    private double soldAvailable;
    
    public void printReceipt(String nameClient,double soldAvailable) {
        String receiptDrawed = "|‾" + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾|\n"
                + "|\t    " + nameClient + "\t   |\n"
                + "|" + "\t\t\t\t   |\n"
                + "|" + "\t\t\t\t   |\n"
                + "|" + "" + "\t\t   " + soldAvailable + "\t           |\n"
                + "|_ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _|";
        System.out.println(receiptDrawed);
    }

    public double getSoldAvailable() {
        return soldAvailable;
    }

    public final void setSoldAvailable(double soldAvailable) {
        this.soldAvailable = soldAvailable;
    }

    public String getNameClient() {
        return nameClient;
    }

    public final void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public Receipt() {
        nameClient = "";
        soldAvailable = 0;
    }

    public Receipt(String nameClient, double soldAvailable) {
        this.setNameClient(nameClient); 
        this.setSoldAvailable(soldAvailable);
    }          
}
