/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

/**
 *
 * @author Florin0
 */
public class Card {

    private Bank entityBank;
    private String typeCard;
    private String titularCont;
    private short pin;
    private double moneyAvailable;
    private String dataExpired;
    private int id;

    public int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    public String getDataExpired() {
        return dataExpired;
    }

    public final void setDataExpired(String dataExpired) {
        this.dataExpired = dataExpired;
    }

    public Bank getEntityBank() {
        return entityBank;
    }

    public final void setEntityBank(Bank entityBank) {
        this.entityBank = entityBank;
    }

    public String getTypeCard() {
        return typeCard;
    }

    public final void setTypeCard(String typeCard) {
        this.typeCard = typeCard;
    }

    public Card(Bank entityBank, String name, String typeCard, short pin, double moneyAvailable, String dataExpired, int id) {
        this.setEntityBank(entityBank);
        this.setName(name);
        this.setTypeCard(typeCard);
        this.setPin(pin);
        this.setMoneyAvailable(moneyAvailable);
        this.setDataExpired(dataExpired);
        this.setId(id);
    }          

    public Card() {
        entityBank = null;
        titularCont = "";
        typeCard = "";
        pin = 0;
        moneyAvailable = 0;
        dataExpired = null;
    }

    public short getPin() {
        return pin;
    }

    public final void setPin(short pin) {
        this.pin = pin;
    }

    public double getMoneyAvailable() {
        return moneyAvailable;
    }

    public final void setMoneyAvailable(double moneyAvailable) {
        this.moneyAvailable = moneyAvailable;
    }

    public String getName() {
        return titularCont;
    }

    public final void setName(String name) {
        this.titularCont = name;
    }

    @Override
    public String toString() {
        String cardDrawed = "|‾" + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾ " + " ‾|\n"
                + "|\t           " + this.getEntityBank() + "\t\t   |\n"
                + "|" + "\t\t\t\t   |\n"
                + "|" + "\t\t\t\t   |\n"
                + "|" + this.getName() + "\t\t   " + this.getDataExpired() + " |\n"
                + "|_ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _ " + " _|";
        return cardDrawed;
    }
}
