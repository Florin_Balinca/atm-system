/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

/**
 *
 * @author Florin0
 */
public class Bank {

    private String nameBank;

    public String getNameBank() {
        return nameBank;
    }

    public final void setNameBank(String nameBank) {
        this.nameBank = nameBank;
    }

    public Bank(String nameBank) {
        this.setNameBank(nameBank);
    }

    @Override
    public String toString() {
        return this.getNameBank();
    }
}
