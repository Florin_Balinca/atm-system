/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

/**
 *
 * @author Florin0
 */
public class Money {

    private String currency;
    private double value;

    public String getCurrency() {
        return currency;
    }

    public Money(String currency, double value) {
        this.setCurrency(currency);
        this.setValue(value);
    }

    public final void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getValue() {
        return value;
    }

    public final void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getValue() + this.getCurrency();
    }
}
