/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.business;

/**
 *
 * @author Florin0
 */
public class Client {

    private Card card;
    private String nameClient;

    public Card getCard() {
        return card;
    }

    public final void setCard(Card card) {
        this.card = card;
    }

    public Client(Card card, String nameClient) {
        this.setCard(card);
        this.setNameClient(nameClient);
    }

    public String getNameClient() {
        return nameClient;
    }

    public final void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    @Override
    public String toString() {
        return this.getCard() + this.getNameClient();
    }
}
