/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.device.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Florin0
 */
public class Keyboard {

    public short readPin() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String shortValue = bf.readLine();
        short pin = Short.parseShort(shortValue);
        return pin;
    }
    
    public double readValue() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String doubleValue = br.readLine();
        double value = Double.parseDouble(doubleValue);
        return value;
    }
    
    public int readOption() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String optionValue = br.readLine();
        int option = Integer.parseInt(optionValue);
        return option;
    }
}
