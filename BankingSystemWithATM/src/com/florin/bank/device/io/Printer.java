/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.device.io;

import com.florin.bank.business.Receipt;

/**
 *
 * @author Florin0
 */
public class Printer {
    public void freeReceipt(Receipt receipt) {        
        receipt.printReceipt(receipt.getNameClient(),receipt.getSoldAvailable());
    }
}
