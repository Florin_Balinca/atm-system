/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.bank.device.io;

import com.florin.bank.transaction.Transaction;

/**
 *
 * @author Florin0
 */
public class Display {

    public Transaction[] transaction = new Transaction[3];
    public Double[] doubleValues = new Double[5];
    public String text;

    public Display() {
        transaction[0] = new Transaction("Withdraw founds");
        transaction[1] = new Transaction("Query Sold");
        transaction[2] = new Transaction("Transfer sum");
        doubleValues[0] = new Double(10);
        doubleValues[1] = new Double(20);
        doubleValues[2] = new Double(50);
        doubleValues[3] = new Double(100);
        doubleValues[4] = new Double(500);
        this.text = "Other sum";
    }

    public void displayTransactions(Transaction[] transactions) {
        int i;
        for (i = 0; i < transactions.length; i++) {
            System.out.println(i + 1 + "->" + transactions[i].getName());
        }
    }

    public void displaySum(Double[] doubles) {
        int i;
        for (i = 0; i < doubles.length; i++) {
            System.out.println(i + 1 + "->" + doubles[i]);
        }
    }

    public void displayOtherSum(String strings) {
        System.out.println(doubleValues.length + 1 + "->" + strings);
    }
}
