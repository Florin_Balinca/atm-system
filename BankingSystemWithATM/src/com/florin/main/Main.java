/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.florin.main;

import com.florin.bank.business.Atm;
import com.florin.bank.business.Card;
import com.florin.bank.business.CardAndMoney;
import com.florin.bank.business.Client;
import com.florin.bank.business.Receipt;
import com.florin.bank.console.AtmConsoleUI;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Florin0
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {        
        AtmConsoleUI console = new AtmConsoleUI();
        System.out.print("Given the id of your card: ");
        int idCard = console.readIdOfCard();
        Card card = console.initCard(idCard);
        System.out.println("Enter your card here:");
        Client client = new Client(card, card.getName());
        Atm.atm.cardReader.readCard(client.getCard());
        System.out.println(client.getCard());
        if (Atm.atm.isCardInDatabase(client.getCard())) {
            System.out.print("Enter you the pin of card: ");
            short pinOfCard = Atm.atm.keyboard.readPin();
            if (pinOfCard == client.getCard().getPin()) {
                int option = 0;
                do {
                    Atm.atm.display.displayTransactions(Atm.atm.display.transaction);
                    System.out.print("Choose a transaction: ");
                    option = Atm.atm.keyboard.readOption();
                    switch (option) {
                        case 1:
                            Atm.atm.display.displaySum(Atm.atm.display.doubleValues);
                            Atm.atm.display.displayOtherSum(Atm.atm.display.text);
                            System.out.print("Withdraw founds, select a value for withdrawing: ");
                            option = Atm.atm.keyboard.readOption();
                            switch (option) {
                                case 1:
                                    CardAndMoney cardAndMoney_10 = Atm.atm.withdrawCash(client.getCard(), 10.0);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoney_10.getCash());
                                    System.out.println("Take your receipt: ");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - 10.0));
                                    break;
                                case 2:
                                    CardAndMoney cardAndMoney_20 = Atm.atm.withdrawCash(client.getCard(), 20.0);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoney_20.getCash());
                                    System.out.println("Take your receipt: ");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - 20.0));
                                    break;
                                case 3:
                                    CardAndMoney cardAndMoney_50 = Atm.atm.withdrawCash(client.getCard(), 50.0);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoney_50.getCash());
                                    System.out.println("Take your receipt: ");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - 50.0));
                                    break;
                                case 4:
                                    CardAndMoney cardAndMoney_100 = Atm.atm.withdrawCash(client.getCard(), 100.0);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoney_100.getCash());
                                    System.out.println("Take your receipt: ");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - 100.0));
                                    break;
                                case 5:
                                    CardAndMoney cardAndMoney_500 = Atm.atm.withdrawCash(client.getCard(), 500.0);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoney_500.getCash());
                                    System.out.println("Take your receipt: ");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - 500.0));
                                    break;
                                case 6:
                                    System.out.println("Given the withdraw value:");
                                    double withdrawValue = Atm.atm.keyboard.readValue();
                                    CardAndMoney cardAndMoneyOtherSum = Atm.atm.withdrawCash(client.getCard(), withdrawValue);
                                    System.out.println("You have withdrawed the sum: " + cardAndMoneyOtherSum.getCash());
                                    System.out.println("Take your receipt:");
                                    Atm.atm.printer.freeReceipt(new Receipt(client.getNameClient(), client.getCard().getMoneyAvailable() - withdrawValue));
                                    break;
                                default:
                                    System.out.println("Wrong option!");
                                    break;
                            }
                            break;
                        case 2:
                            System.out.println("Querry Balance");
                            Atm.atm.querryBalance(client.getCard().getMoneyAvailable());
                            break;
                        case 3:
                            System.out.print("Transfer money, read the value for transfer: ");
                            double transferValue = Atm.atm.keyboard.readValue();
                            System.out.print("Given the id for destination card: ");
                            idCard = console.readIdOfCard();
                            Card cardDestination = console.initCard(idCard);
                            Atm.atm.transferMoney(card, transferValue, cardDestination);
                            break;
                        default:
                            System.out.println("Thank you for using our servicies, please pick up your card!\n" + Atm.atm.cardReader.returnCard(card));
                            break;
                    }
                } while (option <= 3 && option != 0);
            }
        }
    }
}
